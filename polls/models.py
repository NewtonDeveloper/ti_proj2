from django.db import models

# Create your models here.

from django.db import models
from django.contrib.auth.models import User


class Pogoda(models.Model):
    data = models.DateField()
    czas = models.TimeField()
    miejsce = models.CharField(max_length=100)
    tmax = models.CharField(max_length=100)
    tmin = models.CharField(max_length=100)
    cisnienie = models.CharField(max_length=100)
    wiatr = models.CharField(max_length=100)
    porywy = models.CharField(max_length=100)
    opad = models.CharField(max_length=100)
    zachmurzenie = models.CharField(max_length=100)
    class Meta:
        unique_together = ('data', 'czas','miejsce')
        index_together = [
            ["data", "czas", "miejsce"],
        ]

    def __str__(self):
        return self.question


