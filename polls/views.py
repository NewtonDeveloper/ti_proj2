from django.shortcuts import render
import json
# Create your views here.

from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .models import *
from django.views import generic

from django.contrib.auth.views import login

from polls.chart import pogoda_chart
from django.core.validators import *



def register(request):
    if request.method == 'POST':
        arg_username = request.POST.get("username")
        arg_password = request.POST.get("password")
        arg_email = request.POST.get("email")
        user = User(  email=arg_email,username=arg_username)
        user.set_password(arg_password)
        user.save()
    return render(request, 'rejestracja.html')

def validate_pogoda_form(arg_data, arg_czas, arg_miejsce, arg_tmax, arg_tmin, arg_cisnienie, arg_wiatr, arg_porywy, arg_opad, arg_zachmurzenie):
    
    if arg_data == "":
        raise ValidationError('Pole Data jest niepoprawne')

    if arg_czas == "":
        raise ValidationError('Pole Czas jest niepoprawne')
 
    if arg_miejsce == "":
        raise ValidationError('Pole Miejsce jest niepoprawne')
 
    MinValueValidator(-200)(int(arg_tmax))
    MaxValueValidator(200)(int(arg_tmax))

    MinValueValidator(-200)(int(arg_tmin))
    MaxValueValidator(200)(int(arg_tmin))

    if int(arg_tmax) < int(arg_tmin):
        raise ValidationError('Błąd: Tmin jest większy od Tmax')
   
    MinValueValidator(500)(int(arg_cisnienie))
    MaxValueValidator(1500)(int(arg_cisnienie))
 
    MinValueValidator(0)(int(arg_wiatr))
    MaxValueValidator(500)(int(arg_wiatr))
    
    MinValueValidator(0)(int(arg_porywy))
    MaxValueValidator(1000)(int(arg_porywy))

    if int(arg_wiatr) > int(arg_porywy):
        raise ValidationError('Błąd: Wiatr jest większy od Porywy')

    MinValueValidator(0)(int(arg_opad))
    MaxValueValidator(1000)(int(arg_opad))

    MinValueValidator(0)(int(arg_zachmurzenie))
    MaxValueValidator(100)(int(arg_zachmurzenie))
    
def dokumentacja(request):

    return render(request, 'dokumentacja.html')
 
 
def index(request):

    if request.user.is_authenticated and request.method == 'POST':
        arg_data = request.POST.get("data")
        arg_czas = request.POST.get("czas")
        arg_miejsce = request.POST.get("miejsce")
        arg_tmax = request.POST.get("tmax")
        arg_tmin = request.POST.get("tmin")
        arg_cisnienie = request.POST.get("cisnienie")
        arg_wiatr = request.POST.get("wiatr")
        arg_porywy = request.POST.get("porywy")
        arg_opad = request.POST.get("opad")
        arg_zachmurzenie = request.POST.get("zachmurzenie")

        if request.META['CONTENT_TYPE']\
            == "application/json":
            print('Raw Data: "%s"' % request.body)   
            result = json.loads(str(request.body.decode("utf-8")))
            arg_data = result["data"]
            arg_czas = result["czas"]
            arg_miejsce = result["miejsce"]
            arg_tmax = result["tmax"]
            arg_tmin = result["tmin"]
            arg_cisnienie = result["cisnienie"]
            arg_wiatr = result["wiatr"]
            arg_porywy = result["porywy"]
            arg_opad = result["opad"]
            arg_zachmurzenie = result["zachmurzenie"]

        validate_pogoda_form(arg_data, arg_czas, arg_miejsce, arg_tmax, arg_tmin, arg_cisnienie, arg_wiatr, arg_porywy, arg_opad, arg_zachmurzenie)
        pogoda_rec =Pogoda( data=arg_data, czas=arg_czas, miejsce=arg_miejsce, tmax=arg_tmax, tmin=arg_tmin, cisnienie=arg_cisnienie, wiatr=arg_wiatr, porywy=arg_porywy, opad=arg_opad, zachmurzenie=arg_zachmurzenie)
        pogoda_rec.save()

    return render(request, 'index.html')
    
class przegladajListView(generic.ListView):
    model=Pogoda
    template_name = "przegladaj.html"
   
    def get_context_data(self, **kwargs):
        context = super(przegladajListView, self).get_context_data(**kwargs)

        context['miejsce_list'] = Pogoda.objects.values('miejsce').distinct()
        return context


def zaloguj(request):
    m = User.objects.get(username=request.POST['username'])
    if m.password == request.POST['password']:
        request.session['member_id'] = m.id
        return HttpResponse("You're logged in.")
    else:
        return HttpResponse("Your username and password didn't match.")


