# Generated by Django 2.0.10 on 2019-01-25 20:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pogoda',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField()),
                ('czas', models.TimeField()),
                ('miejsce', models.CharField(max_length=100)),
                ('tmax', models.CharField(max_length=100)),
                ('tmin', models.CharField(max_length=100)),
                ('cisnienie', models.CharField(max_length=100)),
                ('wiatr', models.CharField(max_length=100)),
                ('porywy', models.CharField(max_length=100)),
                ('opad', models.CharField(max_length=100)),
                ('zachmurzenie', models.CharField(max_length=100)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='pogoda',
            unique_together={('data', 'czas', 'miejsce')},
        ),
        migrations.AlterIndexTogether(
            name='pogoda',
            index_together={('data', 'czas', 'miejsce')},
        ),
    ]
