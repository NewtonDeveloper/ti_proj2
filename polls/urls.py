from django.urls import path

from .views import  *

from django.contrib.auth.views import logout

from polls.chart import pogoda_chart


urlpatterns = [
    path('pogoda/wykres/', pogoda_chart.chart, name='chart'),
    path("pogoda/przegladaj/", przegladajListView.as_view(), name="przegladaj"),
    path("pogoda/zaloguj/", login,{'template_name': 'zaloguj.html'}, name="zaloguj"),
    path('pogoda/logout/', logout, {'next_page': '/pogoda'}, name="logout"),
    path("pogoda/rejestracja/", register, name="rejestracja"),
    path("pogoda/", index, name="index"),
    path("pogoda/dokumentacja/", dokumentacja, name="dokumentacja"),

]

