from django.shortcuts import render
from django.http import HttpResponse
import datetime

# Include the `fusioncharts.py` file which has required functions to embed the charts in html page
from ..fusioncharts import FusionCharts
from ..models import *

# Loading Data from a Static JSON String
# It is a example to show a spline chart where data is passed as JSON string format.
# The `chart` method is defined to load chart data from an JSON string.

def chart(request):
    # Create an object for the spline chart using the FusionCharts class constructor

    if request.method == 'POST':
        arg_miejsce = request.POST.get("miejsce")
        print("dane:")
 

    data_source={}
    data_source['chart']={}
    data_source['chart']['caption']="Temperatura maksymalna"
    data_source['chart']['subCaption']="Odczyt z bazy SQlite3"
    data_source['chart']['xAxisName']="Dzień"
    data_source['chart']['yAxisName']="Temperatura [C]"
    data_source['chart']['theme']="fusion"

    data_source['data']=[]


    
    database_result=Pogoda.objects.all()
    for rec in database_result.values("data","tmax","tmin","miejsce"):
        if rec['miejsce'] == arg_miejsce:
            print(rec)
 
    for rec in database_result.values("data","czas","tmax","tmin","miejsce"):
        if rec['miejsce'] == arg_miejsce: 
            d={}
            d['label']=str(rec['data'])+" "+str(rec['czas'])
            d['value']=rec['tmax']
            data_source['data'].append(d)

    onchart = str(data_source)


    spline = FusionCharts("spline", "ex1", 600, 400, "chart-1", "json", onchart)
          # The chart data is passed as a string to the `dataSource` parameter.
     # returning complete JavaScript and HTML code, which is used to generate chart in the browsers. 
    return  render(request, 'fusion.html', {'output' : spline.render(),'chartTitle': arg_miejsce+' dane pogodowe'})
